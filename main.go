package main

import (
	"encoding/json"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type Product struct {
	Id      int
	Model   string
	Company string
	Price   int `db:"price"`
}

var db *sqlx.DB

func main() {
	db = sqlx.MustConnect("sqlite3", "store.db")
	// defer db.Close()

	result := db.MustExec(`
	CREATE TABLE IF NOT EXISTS products(
		id INTEGER PRIMARY KEY AUTOINCREMENT, 
		model TEXT,
		company TEXT,
		price INTEGER
	  );	
	`)

	result = db.MustExec("insert into products (model, company, price) values ('iPhone X', $1, $2)",
		"Apple", 72000)

	fmt.Println(result.LastInsertId()) // id последнего добавленного объекта
	fmt.Println(result.RowsAffected()) // количество добавленных строк

	pp := []Product{}

	err := db.Select(&pp, "select * from products")
	if err != nil {
		println(err.Error())
	}

	bytes, err := json.MarshalIndent(pp, "", "  ")

	println(string(bytes))

}
